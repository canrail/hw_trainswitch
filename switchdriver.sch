EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:PL_connectors
LIBS:lpc11c24
LIBS:con-wago
LIBS:canrail_hw_switch-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Switchdriver for model train switches"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FDS6890A Q1
U 2 1 5661310C
P 6150 4100
AR Path="/566126D4/5661310C" Ref="Q1"  Part="2" 
AR Path="/56613B07/5661310C" Ref="Q2"  Part="2" 
AR Path="/56613EA0/5661310C" Ref="Q3"  Part="2" 
AR Path="/56613EA6/5661310C" Ref="Q4"  Part="2" 
F 0 "Q1" H 6400 4175 50  0000 L CNN
F 1 "FDS6984AS" H 6400 4100 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 6400 4025 50  0001 L CIN
F 3 "" H 6150 4100 50  0000 L CNN
F 4 "2454158" H 6150 4100 60  0001 C CNN "farnell"
	2    6150 4100
	1    0    0    -1  
$EndComp
$Comp
L FDS6890A Q1
U 1 1 5661310D
P 5500 3800
AR Path="/566126D4/5661310D" Ref="Q1"  Part="1" 
AR Path="/56613B07/5661310D" Ref="Q2"  Part="1" 
AR Path="/56613EA0/5661310D" Ref="Q3"  Part="1" 
AR Path="/56613EA6/5661310D" Ref="Q4"  Part="1" 
F 0 "Q1" H 5750 3875 50  0000 L CNN
F 1 "FDS6984AS" H 5750 3800 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5750 3725 50  0001 L CIN
F 3 "" H 5500 3800 50  0000 L CNN
F 4 "2454158" H 5500 3800 60  0001 C CNN "farnell"
	1    5500 3800
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5661310F
P 5050 4400
AR Path="/566126D4/5661310F" Ref="R2"  Part="1" 
AR Path="/56613B07/5661310F" Ref="R4"  Part="1" 
AR Path="/56613EA0/5661310F" Ref="R6"  Part="1" 
AR Path="/56613EA6/5661310F" Ref="R8"  Part="1" 
F 0 "R2" V 5130 4400 50  0000 C CNN
F 1 "10k" V 5050 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4980 4400 50  0001 C CNN
F 3 "" H 5050 4400 50  0000 C CNN
	1    5050 4400
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 56613110
P 5400 4400
AR Path="/566126D4/56613110" Ref="R3"  Part="1" 
AR Path="/56613B07/56613110" Ref="R5"  Part="1" 
AR Path="/56613EA0/56613110" Ref="R7"  Part="1" 
AR Path="/56613EA6/56613110" Ref="R9"  Part="1" 
F 0 "R3" V 5480 4400 50  0000 C CNN
F 1 "10k" V 5400 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5330 4400 50  0001 C CNN
F 3 "" H 5400 4400 50  0000 C CNN
	1    5400 4400
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky_x2_KCom_AKA D1
U 1 1 56613111
P 6200 3150
AR Path="/566126D4/56613111" Ref="D1"  Part="1" 
AR Path="/56613B07/56613111" Ref="D2"  Part="1" 
AR Path="/56613EA0/56613111" Ref="D3"  Part="1" 
AR Path="/56613EA6/56613111" Ref="D4"  Part="1" 
F 0 "D1" H 6250 3050 50  0000 C CNN
F 1 "RB085B-30TL" H 6200 3250 50  0000 C CNN
F 2 "dpak:TO-252-3" H 6200 3150 50  0001 C CNN
F 3 "" H 6200 3150 50  0000 C CNN
F 4 "2143985" H 6200 3150 60  0001 C CNN "farnell"
	1    6200 3150
	-1   0    0    1   
$EndComp
Text HLabel 4800 3850 0    60   Input ~ 0
IN_A
Text HLabel 4800 4150 0    60   Input ~ 0
IN_B
Text HLabel 5950 2800 1    60   Input ~ 0
VCC
Text HLabel 5950 4750 3    60   Input ~ 0
GND
$Comp
L CP C10
U 1 1 5661BB7F
P 6950 4150
AR Path="/566126D4/5661BB7F" Ref="C10"  Part="1" 
AR Path="/56613B07/5661BB7F" Ref="C11"  Part="1" 
AR Path="/56613EA0/5661BB7F" Ref="C12"  Part="1" 
AR Path="/56613EA6/5661BB7F" Ref="C13"  Part="1" 
F 0 "C10" H 6975 4250 50  0000 L CNN
F 1 "1u" H 6975 4050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6988 4000 30  0001 C CNN
F 3 "" H 6950 4150 60  0000 C CNN
	1    6950 4150
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 5661310B
P 7250 3500
AR Path="/566126D4/5661310B" Ref="P2"  Part="1" 
AR Path="/56613B07/5661310B" Ref="P3"  Part="1" 
AR Path="/56613EA0/5661310B" Ref="P4"  Part="1" 
AR Path="/56613EA6/5661310B" Ref="P5"  Part="1" 
F 0 "P2" H 7250 3700 50  0000 C CNN
F 1 "CONN_01X03" V 7350 3500 50  0000 C CNN
F 2 "PL_connectors:TERMINAL_BLOCK_2_5mm" H 7250 3500 50  0001 C CNN
F 3 "" H 7250 3500 50  0000 C CNN
F 4 "2396254" H 7250 3500 60  0001 C CNN "farnell"
	1    7250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3900 6250 3600
Wire Wire Line
	6250 3600 7050 3600
Wire Wire Line
	6350 3900 6350 3600
Connection ~ 6350 3600
Wire Wire Line
	5600 3600 5600 3400
Wire Wire Line
	5600 3400 7050 3400
Wire Wire Line
	5700 3600 5700 3400
Connection ~ 5700 3400
Wire Wire Line
	5600 4650 5600 4000
Wire Wire Line
	5050 4650 6950 4650
Wire Wire Line
	6250 4650 6250 4300
Wire Wire Line
	5950 4750 5950 4650
Connection ~ 5950 4650
Wire Wire Line
	4800 4150 5950 4150
Wire Wire Line
	4800 3850 5300 3850
Wire Wire Line
	5400 4650 5400 4550
Connection ~ 5600 4650
Wire Wire Line
	5050 4650 5050 4550
Connection ~ 5400 4650
Wire Wire Line
	5400 4250 5400 4150
Connection ~ 5400 4150
Wire Wire Line
	5050 4250 5050 3850
Connection ~ 5050 3850
Wire Wire Line
	6500 3150 6600 3150
Wire Wire Line
	6600 3150 6600 3600
Connection ~ 6600 3600
Wire Wire Line
	5800 3400 5800 3150
Wire Wire Line
	5800 3150 5900 3150
Connection ~ 5800 3400
Wire Wire Line
	5950 2800 5950 2900
Wire Wire Line
	6200 2950 6200 2900
Connection ~ 6200 2900
Connection ~ 6250 4650
Wire Wire Line
	5950 2900 6750 2900
Wire Wire Line
	6950 4650 6950 4300
Wire Wire Line
	6750 3500 7050 3500
Wire Wire Line
	6750 2900 6750 3500
Wire Wire Line
	6950 4000 6950 3500
Connection ~ 6950 3500
$EndSCHEMATC
